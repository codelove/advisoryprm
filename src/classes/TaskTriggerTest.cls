@isTest
private class TaskTriggerTest {

    static testMethod void TestPhysicianLastFieldsUpdate() {
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Contact c1 = new Contact();
        c1.FirstName = 'Physician Firstname';
        c1.LastName = 'Physician Lastname';
        c1.AccountId = acc.Id;
        insert c1;
        
        Id visitRecTypeId = [Select Id from RecordType where Name ='Visit' and SobjectType ='Task' limit 1].Id;
        
        CMA_Initiative__c init = new CMA_Initiative__c();
        init.name = 'test initiative';
        insert init;   
       
        Task t1 = new Task();
      
        t1.WhoId = c1.Id;
        t1.ActivityDate = date.today();
        t1.OwnerId = UserInfo.getUserId();
        t1.RecordTypeId = visitRecTypeId;
        t1.whatid = init.id;
        t1.CMAPRM__Outreach_Date_Value__c=Date.today();
        insert t1;       
        
        Task t = new Task();
        t.WhatId = init.id;
        t.Priority = 'Normal';
        t.Status = 'Not Started';
        t.Subject = 'test';
        insert t;
  
        Contact queriedPhysician = [Select Id,CMAPRM_LastVisitedDate__c, CMAPRM_LastVisitedBy__c from Contact where Id=: c1.Id ];
        system.assertEquals(UserInfo.getUserId(), queriedPhysician.CMAPRM_LastVisitedBy__c);
        system.assertEquals(date.today() , queriedPhysician.CMAPRM_LastVisitedDate__c);
        CMA_Initiative__c queriedini = [select Id, CMAPRM__Total_Outreach_Activities__c from CMA_Initiative__c where Id=:init.id ];
        system.assertEquals(2, queriedini.CMAPRM__Total_Outreach_Activities__c);
        
    }
}