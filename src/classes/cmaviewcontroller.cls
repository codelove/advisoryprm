public with sharing class cmaviewcontroller{

    public cmaviewcontroller(){
        seturl();
       
        }
        
   Public String cmaurl{get;set;}
   Public TaskRecordTypes__c CS{get;set;}
   
   
  public void seturl(){ 
   CS= TaskRecordTypes__c.getInstance(userinfo.getUserId());
   
   cmaurl = CS.default_cma_url__c;
   
   if(ApexPages.CurrentPage().getParameters().get('from')=='phys'){
       cmaurl = CS.CMA_url_prefix_for_physician__c + ApexPages.CurrentPage().getParameters().get('pid');
       }
       
   if(ApexPages.CurrentPage().getParameters().get('from')=='init'){
       cmaurl =  CS.CMA_url_prefix_for_initiative__c + ApexPages.CurrentPage().getParameters().get('initid');
       }
       
   }
   
  
          
        
         
    
    }