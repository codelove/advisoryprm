@isTest
private class PostInstallScript_Test {

    static testMethod void myUnitTest() {
        PostInstallScript postinstall = new PostInstallScript();
   		Test.testInstall(postinstall, null);
   		TaskRecordTypes__c settings = TaskRecordTypes__c.getOrgDefaults();
   		system.assertEquals('https://cma.crimsonservices.com/CMADemo/Profiles/Physicians/Summary/', settings.CMA_url_prefix_for_physician__c);
   		system.assertEquals('https://cma.crimsonservices.com/CMADemo/Opportunities/Initiatives/Detail/', settings.CMA_url_prefix_for_initiative__c);
   		system.assertEquals('http://www.advisory.com',settings.Default_CMA_URL__c);
   		RecordType recType = [Select Id from RecordType where Name='Complaint' limit 1];
   		system.assertEquals(settings.ComplaintTaskRecordType__c, recType.Id);
    }
}