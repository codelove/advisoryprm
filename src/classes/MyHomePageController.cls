global with sharing class MyHomePageController {
	
	Public TaskRecordTypes__c CS{get;Set;}
	private Map<Id,Id> complaintsToContacts {
		get {
			if(openComplaints != null && openComplaints.size() > 0) {
				Map<Id,Id> result = new Map<Id,Id>();
				for(Task t: openComplaints) {
					result.put(t.Id, t.WhoId);
				}
				return result;
			}
			return null;
		}		
	}
	
	private Map<Id,Id> complaintsToAccounts {
		get {
			if(openComplaints != null && openComplaints.size() > 0) {
				Map<Id,Id> result = new Map<Id,Id>();
				for(Task t : openComplaints) {
					result.put(t.Id, t.WhatId);
				}
				return result;
			}
			return null;
		}
	}
	public string ComplaintId{get;set;}
	public string PhysicianId{get;set;}
	public string PhysicianAccountId {get;set;}
	public string complaintComments{get;set;}
	private List<Task> openComplaints {get;set;}
	public List<Task> MyTasks {get;set;}
	public Integer MyTasksDueThisMonth {
		get {
			if(MyTasks != null) {
				system.debug('my tasks is not null');
				if(CS == null) CS =  TaskRecordTypes__c.getInstance(userinfo.getUserId());
				List<Task> tasks = new List<Task>();
				
				for(Task t : MyTasks) {
					if(t.ActivityDate.Month() == date.Today().Month() && t.RecordTypeId==CS.VisitTaskRecordType__c) {
						tasks.add(t);
					}
				}
				system.debug('tasks size is: ' + tasks.size());
				return tasks.size();
			}
			
			return null;
		}
	}
	
	public Integer MyTasksDueThisQuarter {
		get {
			if(MyTasks != null) {
				if(CS == null) CS =  TaskRecordTypes__c.getInstance(userinfo.getUserId());
				List<Task> tasks = new List<Task>();
				
				for(Task t : MyTasks) {
					if(Math.floor((t.ActivityDate.Month() + 2) / 3) == Math.floor((Date.Today().Month() + 2) / 3) && t.RecordTypeId==CS.VisitTaskRecordType__c) {
						tasks.add(t);
					}
				}
				system.debug('tasks size is: ' + tasks.size());
				return tasks.size();
			}
			return null;
		}
	}
	
	public Integer MyTasksDueThisYear {
		get {
			if(MyTasks != null) {
				if(CS == null) CS =  TaskRecordTypes__c.getInstance(userinfo.getUserId());
				List<Task> tasks = new List<Task>();
				
				for(Task t : MyTasks) {
					if(t.ActivityDate.Year() == Date.Today().Year() && t.RecordTypeId==CS.VisitTaskRecordType__c) {
						tasks.add(t);
					}
				}
				system.debug('tasks size is: ' + tasks.size());
				return tasks.size();
			}
			return null;
		}
	}
	public Long MonthProgress {
		get {
			if(activityGoal != null && MyTasksDueThisMonth != null) {
				double goal = GetThisMonthGoalValue();
				system.debug('goal value is: ' + goal);
				if(goal != null && goal != 0) {
					Long result = Math.RoundToLong(double.ValueOf(MyTasksDueThisMonth) * 100 /goal);
					system.debug('result is: ' + result);
					return result;
				}
			}
			return null;
		}
		set;
	}

	public Long QuarterProgress {
		get {
			if(activityGoal != null && MyTasksDueThisQuarter != null) {
				double goal = GetThisQuarterGoalValue();
				if(goal != null && goal != 0) {
					Long result = Math.RoundToLong(double.ValueOf(MyTasksDueThisQuarter) * 100 /goal);
					return result;
				}
			}
			return null;
		}
		set;
	}
	
	public Long YearProgress {
		get {
			if(activityGoal != null && MyTasksDueThisYear != null) {
				double goal = GetThisYearGoalValue();
				if(goal != null && goal != 0) {
					Long result = Math.RoundToLong(double.ValueOf(MyTasksDueThisYear) * 100 /goal);
					return result;
				}
			}
			return null;
		}
		set;
	}
	
	private CMAPRM__Activity_Goal__c activityGoal {get;set;}
	
	public MyHomePageController() {
		
			List<CMAPRM__Activity_Goal__c> goals  = [Select Id,CMAPRM__Jan__c,CMAPRM__Feb__c, CMAPRM__Mar__c,CMAPRM__Apr__c, CMAPRM__May__c, CMAPRM__Jun__c, CMAPRM__Jul__c, CMAPRM__Aug__c, CMAPRM__Sep__c, CMAPRM__Oct__c,CMAPRM__Nov__c, CMAPRM__Dec__c, CMAPRM__Type__c, CMAPRM__Goal_Year__c from CMAPRM__Activity_Goal__c where OwnerId=:UserInfo.getUserId() and CMAPRM__Goal_Year__c=:string.valueof(Date.Today().Year())]; 
			system.debug('goals size is: ' + goals.size());
			if(goals != null && goals.size() > 0) {
				activityGoal = goals[0];
			}
			MyTasks = [Select Id, Who.Name, WhoId, status,Description, WhatId, What.Name, Subject, ActivityDate, RecordTypeId from Task where OwnerId = :UserInfo.getUserId() and ActivityDate =THIS_YEAR ];		
			system.debug(upcomingTasksByDate != null);
	}
	
	public Map<String, List<Task>> upcomingTasksByDate {
		get {
			if(UpcomingTasks != null) {
				Map<String, List<Task>> result = new Map<String,List<Task>>();
				for(integer i=0;i < 7;i++) 
				{
					List<Task> daystasks = new List<Task>(); 
					for(Task t: UpcomingTasks) {
						if(t.ActivityDate == Date.Today().AddDays(i)) {
							daystasks.add(t);
						}
					}
					string 	dateFormat =  system.now().addDays(i).format('EEEEE, MMM dd');	
					system.debug('date format is: ' + dateFormat);
					if(i==0) {
						dateFormat = 'TODAY - ' + dateFormat;
					}
					else if(i==1) {
						dateFormat = 'TOMORROW - ' + dateFormat;
					}
					dateFormat = dateFormat.toUpperCase();
					if(daysTasks.size() > 0) {
						result.put(dateFormat, daysTasks);
						if(upcomingTasksDates == null) {
							upcomingTasksDates = new List<String>();
						}
						upcomingTasksDates.add(dateFormat);
						system.debug('added a string to the list');
					}					
				}
				return result;
			}
			return null;	
		}
	}

	public List<String> upcomingTasksDates {get;set;}
	
	public List<Task> getOpenComplaints() {
		CS =  TaskRecordTypes__c.getInstance(userinfo.getUserId());
		openComplaints = [Select Who.Name, Who.FirstName, Who.LastName, WhoId,WhatId, Subject, Priority, CMAPRM__Days_Open__c, CMAPRM__Date_Opened__c, ActivityDate, Description 
		From Task  where Status != 'Completed' and OwnerId = :userInfo.getUserId() and recordtypeid =:CS.ComplaintTaskRecordType__c  order by CMAPRM__Days_Open__c desc limit 10];
		system.debug('number of open complaints:' + openComplaints.size());
		return openComplaints;		
	}
	
	public List<Contact> getLeastVisited() 
	{
		List<Contact> leastVisited = [Select Id, Name,AccountId, CMAPRM__Days_Since_Last_Visit__c From Contact  
		where ownerId =: userInfo.getUserId() and CMAPRM__AttendingRevenueShare__c>=30 and CMAPRM__AttendingRevenueShare__c<=80 
		order by CMAPRM__AttendingMarketLeakage__c desc limit 10];
		//TODO: May be replace this expensive bubblesort by implementing the comparable interface and wrapper class on conatc object.
		for(integer i=0; i<leastVisited.size()-1;++i)
		{
			for(integer j=i+1; j<leastVisited.size(); ++j)
			{
				decimal a=leastVisited[i].CMAPRM__Days_Since_Last_Visit__c==null?0:leastVisited[i].CMAPRM__Days_Since_Last_Visit__c;
				decimal b=leastVisited[j].CMAPRM__Days_Since_Last_Visit__c==null?0:leastVisited[j].CMAPRM__Days_Since_Last_Visit__c;
				if(b<a)
				{
					contact temp=leastVisited[i];
					leastVisited[i]=leastVisited[j];
					leastVisited[j]=temp;
				}
			}
		}
		
		return leastVisited;	
	}
	
	public List<Task> UpcomingTasks {
		get {
			if(MyTasks != null) {
				if(CS == null) CS =  TaskRecordTypes__c.getInstance(userinfo.getUserId());
				List<Task> tasks = new List<Task>();				
				for(Task t : MyTasks) {
					if(t.status!='Completed' && t.ActivityDate >= Date.Today() && t.ActivityDate <= Date.Today().AddDays(6) && t.recordtypeId != CS.CMAPRM__ComplaintTaskRecordType__c && t.recordtypeid != CS.CMAPRM__VisitTaskRecordType__c && t.recordtypeid != CS.CMAPRM__Goal_Record_Type__c) {
						tasks.add(t);
					}
				}
				return tasks;
			}
			return null;
		}
	}
	
	public PageReference CloseComplaint() {
    	if(!String.IsBlank(ComplaintId)) {
    		UpdateComplaintNotesandClose(ComplaintId, null);
    	}
    	return null;
    }
    
    private void UpdateComplaintNotesandClose(string complaintId, string comments ) {
    	if(!String.IsBlank(complaintId)) {
    		Task complaint = new Task();
    		complaint.Id = ComplaintId;
    		complaint.Status = 'Completed';
    		if(!String.IsBlank(comments)) {
    			complaint.Description = comments;
    		}
    		update complaint;
    	}
    }
    
    public PageReference CloseWithNotes() {
    	system.debug('complaint id: ' + ComplaintId);
    	system.debug('complaint comments: ' + complaintComments);
    	UpdateComplaintNotesandClose(ComplaintId, complaintComments);
    	return null;
    }
    
    public PageReference CloseAndCreateNewComplaint() {
    	if(!String.IsBlank(ComplaintId)) {
    		CS =  TaskRecordTypes__c.getInstance(userinfo.getUserId());
    		UpdateComplaintNotesandClose(ComplaintId,null);
    		PageReference pageref = new PageReference('/apex/CMAPRM__log_a_complaint');
    		if(complaintsToContacts != null && complaintsToContacts.containsKey(ComplaintId)) {
    			pageref.getParameters().put('cid',complaintsToContacts.get(ComplaintId));
    		}
    		if(complaintsToAccounts != null && complaintsToAccounts.containsKey(ComplaintId)) {
    			pageref.getParameters().put('accid',complaintsToAccounts.get(ComplaintId));
    		}    		
    		pageref.getParameters().put('rtid',CS.ComplaintTaskRecordType__c);
    		system.debug('returning page ref value: ' + pageref.getUrl());
    		return pageref;
    	}
    	return null;
    }
    
    public PageReference CreateNewVisit() {
    	if(!String.IsBlank(PhysicianId)) {
    		if(CS == null) CS =  TaskRecordTypes__c.getInstance(userinfo.getUserId());
    		PageReference pageref = new PageReference('/apex/CMAPRM__log_a_visit');
    		pageref.getParameters().put('cid',PhysicianId);
    		pageref.getParameters().put('accid',PhysicianAccountId);
    		pageref.getParameters().put('rtid',CS.VisitTaskRecordType__c);
    		system.debug('returning page ref value: ' + pageref.getUrl());
    		return pageref;
    	}
    	return null;
    }
    
    public PageReference CreateNewTask() {
    	string retUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/00T/e';
    	if(!String.IsBlank(PhysicianId)) {
    		retUrl += '?who_id='+PhysicianId;
    	}
    	PageReference pageref = new PageReference(retUrl);
    	return pageref;
    }
    
    private double GetThisMonthGoalValue() {
    	if(activityGoal != null) {
    		
    		string currentMonth = system.now().format('MMMMM');
    		if(currentMonth.startsWith('Jan')) {
    			return activityGoal.CMAPRM__Jan__c;
    		}
    		else if(currentMonth.startsWith('Feb')) {
    			return activityGoal.CMAPRM__Feb__c;
    		}
    		else if(currentMonth.startsWith('Mar')) {
    			return activityGoal.CMAPRM__Mar__c;
    		}
    		else if(currentMonth.startsWith('Apr')) {
    			return activityGoal.CMAPRM__Apr__c;
    		}
    		else if(currentMonth.startsWith('May')) {
    			return activityGoal.CMAPRM__May__c;
    		}
    		else if(currentMonth.startsWith('Jun')) {
    			return activityGoal.CMAPRM__Jun__c;
    		}
    		else if(currentMonth.startsWith('Jul')) {
    			return activityGoal.CMAPRM__Jul__c;
    		}
    		else if(currentMonth.startsWith('Aug')) {
    			return activityGoal.CMAPRM__Aug__c;
    		}
    		else if(currentMonth.startsWith('Sep')) {
    			return activityGoal.CMAPRM__Sep__c;
    		}
    		else if(currentMonth.startsWith('Oct')) {
    			return activityGoal.CMAPRM__Oct__c;
    		}
    		else if(currentMonth.startsWith('Nov')) {
    			return activityGoal.CMAPRM__Nov__c;
    		}
    		else if(currentMonth.startsWith('Dec')) {
    			return activityGoal.CMAPRM__Dec__c;
    		}
    		
    	}
    	return null;
    }
    
    private double GetThisQuarterGoalValue() {
    	if(activityGoal != null) {
    		double thisQuarter = Math.floor((system.now().Month() + 2) / 3) ;
    		if(thisQuarter == 1) {
    			return SetToZeroIfNull(activityGoal.CMAPRM__Jan__c) + SetToZeroIfNull(activityGoal.CMAPRM__Feb__c) + SetToZeroIfNull(activityGoal.CMAPRM__Mar__c);
    		}
    		else if(thisQuarter == 2) {
    			return SetToZeroIfNull(activityGoal.CMAPRM__Apr__c) + SetToZeroIfNull(activityGoal.CMAPRM__May__c) + SetToZeroIfNull(activityGoal.CMAPRM__Jun__c);
    		}
    		else if(thisQuarter == 3) {
    			return SetToZeroIfNull(activityGoal.CMAPRM__Jul__c) + SetToZeroIfNull(activityGoal.CMAPRM__Aug__c) + SetToZeroIfNull(activityGoal.CMAPRM__Sep__c);
    		}
    		else if(thisQuarter == 4) {
    			return SetToZeroIfNull(activityGoal.CMAPRM__Oct__c) + SetToZeroIfNull(activityGoal.CMAPRM__Nov__c) + SetToZeroIfNull(activityGoal.CMAPRM__Dec__c);
    		}
    	}
    	return null;
    }
    
    private double GetThisYearGoalValue() {
    	if(activityGoal != null) {
    		return SetToZeroIfNull(activityGoal.CMAPRM__Jan__c) + SetToZeroIfNull(activityGoal.CMAPRM__Feb__c) + SetToZeroIfNull(activityGoal.CMAPRM__Mar__c) + SetToZeroIfNull(activityGoal.CMAPRM__Apr__c) + SetToZeroIfNull(activityGoal.CMAPRM__May__c) + SetToZeroIfNull(activityGoal.CMAPRM__Jun__c) + SetToZeroIfNull(activityGoal.CMAPRM__Jul__c) + SetToZeroIfNull(activityGoal.CMAPRM__Aug__c) + SetToZeroIfNull(activityGoal.CMAPRM__Sep__c) + SetToZeroIfNull(activityGoal.CMAPRM__Oct__c) + SetToZeroIfNull(activityGoal.CMAPRM__Nov__c) + SetToZeroIfNull(activityGoal.CMAPRM__Dec__c);
    	}
    	return null;
    }
    
    public double SetToZeroIfNull(decimal input) {
    	if(input == null) {
    		return 0.0;
    	}
    	return input;
    }
}