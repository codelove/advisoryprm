@isTest
private class ContactTriggerTest {

    static testMethod void TestAccPhysicianCount() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
       
        Contact c1 = new Contact();
        c1.AccountId = acc.Id;
        c1.LastName = 'First Contact lastname';
        c1.FirstName = 'First contact firstname';
        insert c1;
        
        Contact c2 = new Contact();
        c2.AccountId = acc.Id;
        c2.LastName = 'Second contact lastname';
        c2.FirstName = 'Second contact firstname';
        insert c2;
        
        Account queriedAccount = [Select Id, PhysicianCount__c from Account where Id=: acc.Id ]; 
        system.assertEquals(2, queriedAccount.PhysicianCount__c);
      
        delete c2;
        
        Account requeriedAccount = [Select Id, PhysicianCount__c from Account where Id=:acc.Id];
        system.assertEquals(1, requeriedAccount.PhysicianCount__c);
        
    }
}