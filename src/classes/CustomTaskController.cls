public with sharing class CustomTaskController{

    public CustomTaskController(ApexPages.StandardController controller){
        createtaskrecord();       
    }
        
   	Public Task mytask{Get;Set;}
   	Public Task newcomplaint{Get;Set;}
   	Public TaskRecordTypes__c CS{Get;Set;}
   
	public void createtaskrecord(){   
 			mytask=new Task();   
		    mytask.OwnerID=userinfo.getUserId();
		    mytask.recordtypeid=ApexPages.CurrentPage().getParameters().get('rtid');
            System.debug('@@@@@ Task Before = ' + mytask);
		    if(!String.IsBlank(ApexPages.CurrentPage().getParameters().get('cid'))) {
		    	system.debug('c id retrived is: ' + ApexPages.CurrentPage().getParameters().get('cid'));
		    	mytask.WhoId = ApexPages.CurrentPage().getParameters().get('cid');
		    }
		    if(!String.IsBlank(ApexPages.CurrentPage().getParameters().get('accid'))) {
		    	system.debug('setting tasks what id to '+ ApexPages.CurrentPage().getParameters().get('accid'));
		    	mytask.WhatId = ApexPages.CurrentPage().getParameters().get('accid');
		    }
		    else if(!String.IsBlank(ApexPages.CurrentPage().getParameters().get('inid'))) {
		    	mytask.WhatId = ApexPages.CurrentPage().getParameters().get('inid');
		    }
		    mytask.Priority = 'Normal';
		    //Set the default status of a visit to completed
		    mytask.Status = 'Completed';

        System.debug('@@@@@ Task After = ' + mytask);
    }    
    
    public void createnewcomplaint(){
           	CS =  TaskRecordTypes__c.getInstance(userinfo.getUserId());
	        newcomplaint=new Task();
	        newcomplaint.OwnerID=userinfo.getuserid();
	        if(!String.IsBlank(ApexPages.CurrentPage().getParameters().get('cid'))) {
	        	newcomplaint.WhoId=ApexPages.CurrentPage().getParameters().get('cid');
	        }
	        if(!String.IsBlank(ApexPages.CurrentPage().getParameters().get('accid'))) {
				newcomplaint.WhatID = ApexPages.CurrentPage().getParameters().get('accid');
			}
			else if(!String.IsBlank(ApexPages.CurrentPage().getParameters().get('inid'))) {
				newcomplaint.WhatID = ApexPages.CurrentPage().getParameters().get('inid');
			}
	        //newcomplaint.WhatID=ApexPages.CurrentPage().getParameters().get('wid');
	        newcomplaint.recordtypeid=CS.ComplaintTaskRecordType__c;  
	        newcomplaint.Priority = 'Normal';
		    newcomplaint.Status = 'Not Started';   
    }    
    
    public pagereference customsave(){
        try {
        	//Set the due date to outreach date by default for visits
        	mytask.ActivityDate = mytask.CMAPRM__Outreach_Date_Value__c;
        	insert mytask;
        }
        catch(Exception e) {
        	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,getErrorMessage(e.getMessage()));
			ApexPages.addMessage(myMsg);
        	return null;
        }
        string returl = '/';
 		if(mytask != null && mytask.WhoId != null) {
 			returl += mytask.WhoId;
 		} 		
 		else if(mytask.WhatId != null) {
      System.debug('@@@@@ WhatId not null');
 			returl += mytask.WhatId;
 		}
        pagereference returnpage = new PageReference(returl);
        return returnpage;
    }
        
    public pagereference saveandlogcomplaint(){
        PageReference returnPage = Page.log_a_complaint;
        returnPage.getParameters().put('accid',ApexPages.CurrentPage().getParameters().get('accid'));
        returnPage.getParameters().put('cid',ApexPages.CurrentPage().getParameters().get('cid')); 
        returnPage.getParameters().put('inid',ApexPages.CurrentPage().getParameters().get('inid'));      
        try {
        	insert newcomplaint; 
        	createnewcomplaint();
        }   
        catch(Exception e) {
        	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,getErrorMessage(e.getMessage()));
			ApexPages.addMessage(myMsg);
        	return null;
        }   
        return returnpage;
    }

    public pageReference saveandlogvisit(){
      PageReference returnPage = Page.log_a_visit;
      returnPage.getParameters().put('cid',ApexPages.CurrentPage().getParameters().get('cid')); 
      returnPage.getParameters().put('inid',ApexPages.CurrentPage().getParameters().get('inid'));
      returnPage.getParameters().put('accid',ApexPages.CurrentPage().getParameters().get('accid'));
      try{
        System.debug('@@@@@ About to insert task... ' + mytask);
        mytask.ActivityDate = mytask.CMAPRM__Outreach_Date_Value__c;
        insert mytask;
        createtaskrecord();
      } catch (Exception e){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,getErrorMessage(e.getMessage()));
        ApexPages.addMessage(myMsg);
        return null;
      }
      return returnPage;
    }
        
   	public pagereference customsavecomplaint(){
        try 
        {
        	newcomplaint.activityDate=newcomplaint.CMAPRM__Outreach_Date_Value__c;
        	insert newcomplaint;
        }
        catch(Exception e) {
        	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,getErrorMessage(e.getMessage()));
			ApexPages.addMessage(myMsg);
        	return null;
        }  
        string returl = '/';
 		if(newcomplaint != null && newcomplaint.WhoId != null) {
 			returl += newcomplaint.WhoId;
 		} 		
 		else if(newcomplaint.WhatId != null) {
 			returl += newcomplaint.WhatId;
 		}
        pagereference returnpage = new PageReference(returl);
        return returnpage;
 	}
 	
 	public string getErrorMessage(string exceptionMessage) {
 		string errorMsg;
       	if(exceptionMessage.contains('Outreach Date value must be in the Past')) {
        	errorMsg = 'Outreach Date value must be in the Past.';
        }
       	else {
       		errorMsg = exceptionMessage;
       	}
       	return errorMsg;
 	}
        
   /*public pagereference saveandlogcomplaintfromphys(){
        PageReference returnPage = Page.log_a_complaint_from_phys;
        returnPage.getParameters().put('cid',mytask.WhoId);
      // string returl='/apex/log_a_complaint_from_phys?cid=' + mytask.WhoId;
       insert mytask;
       
       //pagereference returnpage = new PageReference(returl);
       return returnpage;
 	} 
        
 	public pagereference cancelbacktophysfromvisit(){
        string returl='/' + mytask.WhoId;
        mytask = null;
       
        pagereference returnpage = new PageReference(returl);
        return returnpage;
  	} 
        
 	public pagereference cancelbacktophysfromcomplaint(){
        string returl='/' + newcomplaint.WhoId;
        newcomplaint = null;
       
        pagereference returnpage = new PageReference(returl);
        return returnpage;
 	}*/
 	
 	public PageReference cancelfromvisit() {
 		string returl = '/';
 		if(mytask != null && mytask.WhoId != null) {
 			returl += mytask.WhoId;
 		} 		
 		else if(mytask.WhatId != null) {
 			returl += mytask.WhatId;
 		}
 		myTask = null;
 		pagereference returnpage = new PageReference(returl);
        return returnpage; 		
 	}
 	
 	public PageReference cancelfromcomplaint() {
 		string returl = '/';
 		if(newcomplaint != null && newcomplaint.WhoId != null) {
 			returl += newcomplaint.WhoId;
 		} 		
 		else if(newcomplaint.WhatId != null) {
 			returl += newcomplaint.WhatId;
 		}
 		newcomplaint = null;
 		pagereference returnpage = new PageReference(returl);
        return returnpage;
 	}    
}