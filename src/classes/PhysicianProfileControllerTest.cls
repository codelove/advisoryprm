@isTest
private class PhysicianProfileControllerTest {

    static testMethod void myUnitTest() {
        Account acc = new Account();
        acc.Name ='test account';
        insert acc;
        
        //Test with good data
        Contact c = new Contact();
        c.Lastname = 'tester contact';
        c.AccountId = acc.Id;
        c.CMAPRM__AttendingMemberRevenue__c = 123456;
        c.CMAPRM__AttendingMarketLeakage__c = 12345;
        c.CMAPRM__ReferringMemberRevenue__c = 654321;
        c.CMAPRM__ReferringMarketLeakage__c = 65432;
        c.CMAPRM__AttendingRevenueChange__c = 12345;
        insert c;
        
        ApexPages.StandardController std = new ApexPages.StandardController(c);
        PhysicianProfileController controller = new PhysicianProfileController(std);
        
        system.assertEquals(0, controller.TotalVisits);
        system.assertEquals(0, controller.OpenComplaints);
        system.assertEquals(true, controller.IsYearTrendPositive);
        system.assertEquals(Integer.Valueof(123456*100/(123456 + 12345)), controller.AttendingRevenueShare);
        system.assertEquals(Integer.Valueof(12345 * 100/(12345 + 123456)), controller.AttendingMarketLeakage);
        system.assertEquals(Integer.ValueOf(654321 * 100/(654321 + 65332)), controller.ReferringRevenueShare);
        system.assertEquals(Integer.ValueOf(65432 * 100/(654321 + 65432)), controller.ReferringMarketLeakage);
        system.assertEquals(2, controller.getAttendingBusinessData().size());
        system.assertEquals(2, controller.getReferringBusinessData().size());
        
        //Test with missing data
        c.CMAPRM__AttendingMemberRevenue__c = null;
        c.CMAPRM__AttendingMarketLeakage__c = null;
        c.CMAPRM__ReferringMarketLeakage__c = null;
        c.CMAPRM__ReferringMemberRevenue__c = null;
        c.CMAPRM__AttendingRevenueChange__c = null;
        update c;
        
        ApexPages.StandardController std1 = new ApexPages.StandardController(c);
        PhysicianProfileController controllerObj = new PhysicianProfileController(std1);
        system.assertEquals(null, controllerObj.IsYearTrendPositive);
        system.assertEquals(null, controllerObj.AttendingRevenueShare);
        system.assertEquals(null, controllerObj.AttendingMarketLeakage);
        system.assertEquals(null, controllerObj.ReferringRevenueShare);
        system.assertEquals(null, controllerObj.ReferringMarketLeakage);
        system.assertEquals(1, controllerObj.getAttendingBusinessData().size());
        system.assertEquals(1, controllerObj.getReferringBusinessData().size());
    }
}