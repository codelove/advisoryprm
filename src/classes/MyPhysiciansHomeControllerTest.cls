@isTest
private class MyPhysiciansHomeControllerTest {

    static testMethod void myUnitTest() {
        Account acc = new Account();
        acc.Name = 'Test account';
        
        Contact c = new Contact();
        c.LastName = 'Test user';
        c.AccountId = acc.Id;
        c.OwnerId = UserInfo.getUserId();
        insert c;
        
        myPhysiciansHomeController controller = new myPhysiciansHomeController();
        system.assertEquals(1, controller.contactSet.getResultSize());
        system.assertEquals(1, controller.getContacts().size());
    }
}