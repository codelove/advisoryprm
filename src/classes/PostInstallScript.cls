/****************************************************************
Created: 9 December 2013
Author: Krishna Tatta
Purpose : Post Install Script to setup the custom task record type settings after the package installs
Modifications:
******************************************************************/
global class PostInstallScript implements InstallHandler {
	global void onInstall(InstallContext context) {
  	 	TaskRecordTypes__c settings = new TaskRecordTypes__c(); 
  	 	if(!Test.isRunningTest()) {
  	 		settings = TaskRecordTypes__c.getOrgDefaults(); 
  	 	}
  	 	system.debug('settings id is: ' + settings.Id);
  	 	if(settings.Id == null){
  	 		settings.CMA_url_prefix_for_physician__c = 'https://cma.crimsonservices.com/CMADemo/Profiles/Physicians/Summary/';
  	 		settings.CMA_url_prefix_for_initiative__c = 'https://cma.crimsonservices.com/CMADemo/Opportunities/Initiatives/Detail/';
  	 		settings.Default_CMA_URL__c = 'http://www.advisory.com';
  	 		  	 		
  	 		//grab the Record Type Ids and insert them into task record types settings
  	 		List<RecordType> recTypes = [Select Id, Name from RecordType where Name in ('Goal','Visit','Complaint')];
  	 		if(recTypes.size() > 0){
  	 			for(RecordType rt : recTypes){
  	 				if(rt.Name == 'Complaint'){
  	 					settings.ComplaintTaskRecordType__c = rt.Id;
  	 				}
  	 				else if(rt.Name == 'Goal'){
  	 					settings.Goal_Record_Type__c = rt.Id;
  	 				}
  	 				else if(rt.Name == 'Visit'){
  	 					settings.VisitTaskRecordType__c = rt.Id;
  	 				}
  	 			}
  	 		}  	 		
  	 		insert settings;
  	 	}
    }	
}