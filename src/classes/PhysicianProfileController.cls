public with sharing class PhysicianProfileController {
	
	private final Contact cont;
	public Integer TotalVisits {get; private set;}
	public Integer OpenComplaints {get; private set; }
	
	//Calculate percentage of attending revenue from physician
	public Integer AttendingRevenueShare {
		get {
			if(this.cont != null && this.cont.CMAPRM__AttendingMemberRevenue__c != null && cont.CMAPRM__AttendingMarketLeakage__c != null) {
				return Integer.Valueof(this.cont.CMAPRM__AttendingMemberRevenue__c * 100/(this.cont.CMAPRM__AttendingMemberRevenue__c + this.cont.CMAPRM__AttendingMarketLeakage__c));
			}
			return null;
		}		
	}
	
	//Calculate percentage of referring business revenue from physician
	public Integer ReferringRevenueShare {
		get {
			if(this.cont != null && this.cont.CMAPRM__ReferringMemberRevenue__c != null && cont.CMAPRM__ReferringMarketLeakage__c != null) {
				return Integer.ValueOf(this.cont.CMAPRM__ReferringMemberRevenue__c * 100/(this.cont.CMAPRM__ReferringMemberRevenue__c + this.cont.CMAPRM__ReferringMarketLeakage__c));
			}
			return null;
		}
	}
	
	public Integer AttendingMarketLeakage {
		get {
			if(this.cont != null && this.cont.CMAPRM__AttendingMemberRevenue__c != null && cont.CMAPRM__AttendingMarketLeakage__c != null) {
				return Integer.Valueof(this.cont.CMAPRM__AttendingMarketLeakage__c * 100/(this.cont.CMAPRM__AttendingMemberRevenue__c + this.cont.CMAPRM__AttendingMarketLeakage__c));
			}
			return null;
		}
	}
	
	public Integer ReferringMarketLeakage {
		get {
			if(this.cont != null && this.cont.CMAPRM__ReferringMemberRevenue__c != null && cont.CMAPRM__ReferringMarketLeakage__c != null) {
				return Integer.ValueOf(this.cont.CMAPRM__ReferringMarketLeakage__c * 100/(this.cont.CMAPRM__ReferringMemberRevenue__c + this.cont.CMAPRM__ReferringMarketLeakage__c));
			}
			return null;
		}
	}
	
	public PhysicianProfileController(ApexPages.StandardController stdController) {
		Contact c = (Contact)stdController.getRecord();
		this.cont = [select Id, CMAPRM__AttendingMemberRevenue__c,CMAPRM__ReferringMemberRevenue__c,CMAPRM__AttendingMarketLeakage__c,CMAPRM__ReferringMarketLeakage__c, CMAPRM__AttendingRevenueChange__c from Contact where Id=:c.Id];
		TaskRecordTypes__c CS = TaskRecordTypes__c.getInstance(userinfo.getUserId());
		TotalVisits = [Select COUNT() from Task Where RecordTypeId =: CS.VisitTaskRecordType__c and WhoId =: cont.Id ];
		OpenComplaints = [Select Count() from Task where RecordTypeId =: CS.ComplaintTaskRecordType__c and WhoId=: cont.Id and Status != 'Completed'];
	}
	
	//build data for pie chart with attending member revenue and attending market leakage percentages
	public List<PieWedgeData> getAttendingBusinessData() {
		List<PieWedgeData> data = new List<PieWedgeData>();
        if(AttendingRevenueShare != null && AttendingMarketLeakage != null) {
	        data.add(new PieWedgeData('Revenue Share', AttendingRevenueShare));
	        data.add(new PieWedgeData('Market Leakage', AttendingMarketLeakage));
        }
        else {
        	data.add(new PieWedgeData('No data', 100));
        }
        return data;
	}
	
	//build data for pie chart with referring member revenue and referring market leakage percentages
	public List<PieWedgeData> getReferringBusinessData() {
		List<PieWedgeData> data = new List<PieWedgeData>();
		if(ReferringRevenueShare != null && ReferringMarketLeakage != null) {
			data.add(new PieWedgeData('Revenue Share', ReferringRevenueShare));
			data.add(new PieWedgeData('Market Leakage', ReferringMarketLeakage));
		}
		 else {
        	data.add(new PieWedgeData('No data', 100));
        }
		return data;		
	}
	
	public boolean IsYearTrendPositive {
		get {
			if(this.cont.CMAPRM__AttendingRevenueChange__c != null) {				
				boolean result = (this.cont.CMAPRM__AttendingRevenueChange__c >= 0);
				system.debug('year trend growth is: ' + result);
				return result;
			}
			system.debug('returning null');
			return null;
			
		}
	}
	
	// Wrapper class
    public class PieWedgeData
    {

        public String name { get; set; }
        public Integer data { get; set; }

        public PieWedgeData(String name, Integer data) {
            this.name = name;
            this.data = data;
        }
    }
    public class StackedBarData {

        public string revenue{ get; set; }
        public string leakage { get; set; }

        public StackedBarData(String revenue, string leakage) 
        {
            this.revenue= revenue;
            this.leakage=leakage;
        }
    }
    
}