public with sharing class UtilsRecordTypes 
{
	static Map<string, id> recordType_Id_Map=null;
	static Map<Id, string> Id_recordType_Map=null;
	
	static public Map<string, id> getMapRecordTypeNmAndId(string sOjbNm)
	{
        recordType_Id_Map =new Map<string, id>();
        List<RecordType> recordTypes = [Select id, name from RecordType where sObjectType=: sOjbNm];
        for(RecordType rt: recordTypes)
        {
            recordType_Id_Map.put(rt.Name, rt.Id);
        }
        return recordType_Id_Map;
	}

	static public Map<Id, string> getMapIdAndRecordTypeNm(string sOjbNm)
	{
		if(Id_recordType_Map == null)
        {
            Id_recordType_Map =new Map<Id, string>();
            List<RecordType> recordTypes = [Select id, name from RecordType where sObjectType=: sOjbNm];
            for(RecordType rt: recordTypes)
            {
                Id_recordType_Map.put(rt.Id,rt.Name);
            }
        }
        return Id_recordType_Map;
	}
}