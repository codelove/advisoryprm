@isTest
private class MyHomePageControllerTest {
    static Id ComplaintRecordTypeId;
    static Id VisitRecordTypeId;
    static Id GoalRecordTypeId;
    
    @isTest
    static void TestController() {
            
            setRecordTypes();   
	        Account acc = new Account();
	        acc.Name = 'Testaccount';
	        
	        Contact con = new Contact();
	        con.LastName = 'testcontact';
	        con.CMAPRM__AttendingRevenueShare__c=40;
	        insert con;	       
	        
	        List<Task> tasks = new List<Task>();
	        Task newVisit = new Task();
		    newVisit.WhoId = con.Id;
	        newVisit.WhatId = acc.Id;
	        newVisit.RecordTypeId = VisitRecordTypeId;
	        newVisit.OwnerID=Userinfo.getUserId();
	        newVisit.Priority = 'Normal';
			newVisit.Status = 'Not Started';
			newVisit.ActivityDate = date.Today();
			tasks.add(newVisit);
			
			Task anotherVisit = new Task();
		    anotherVisit.WhoId = con.Id;
	        anotherVisit.WhatId = acc.Id;
	        anotherVisit.RecordTypeId = VisitRecordTypeId;
	        anotherVisit.OwnerID=Userinfo.getUserId();
	        anotherVisit.Priority = 'Normal';
			anotherVisit.Status = 'Not Started';
			anotherVisit.ActivityDate = date.Today();	
			tasks.add(anotherVisit);
		
	        Task newComplaint = new Task();			
			newComplaint.OwnerId = UserInfo.getUserId();
			newComplaint.RecordTypeId = ComplaintRecordTypeId;
			system.debug('task record type id is: ' + ComplaintRecordTypeId);
			newComplaint.WhoId = con.Id;
			newComplaint.WhatId = acc.Id;
			newComplaint.Priority = 'Normal';
			newComplaint.Status = 'Not Started';
			newComplaint.ActivityDate = date.Today();
	        tasks.add(newComplaint);    		
    				
			insert tasks;
    		
	        MyHomePageController controllerObj = new MyHomePageController();
	        system.assertEquals(3, controllerObj.MyTasks.size());
	        system.assertEquals(null, controllerObj.MonthProgress);
	        system.assertEquals(null, controllerObj.QuarterProgress);
	        system.assertEquals(null, controllerObj.YearProgress);
        	system.assertEquals(1, controllerObj.upcomingTasksByDate.size());
        	system.assertEquals(0, controllerObj.MyTasksDueThisMonth);
        	system.assertEquals(0, controllerObj.MyTasksDueThisQuarter);
        	system.assertEquals(0, controllerObj.MyTasksDueThisYear);
        	//system.assertEquals(2, controllerObj.upcomingTasksByDate.get('TODAY - ' + system.now().format('EEEEE, MMM dd')).size());
        	system.assertEquals(0, controllerObj.getOpenComplaints().size());
        	system.assertEquals(1, controllerObj.getLeastVisited().size());
        	system.assertEquals(3, controllerObj.UpcomingTasks.size());
        	controllerObj.ComplaintId = newComplaint.Id;
        	controllerObj.CloseComplaint();
        	Task queriedComplaint = [select Id, Status from Task where Id=: newComplaint.Id];
        	system.assertEquals('Completed', queriedComplaint.Status);
        	controllerObj.ComplaintId = newComplaint.Id;
        	controllerObj.complaintComments = 'new comments';
        	controllerObj.CloseWithNotes();
        	Task requeriedComplaint = [select Id, Status, Description from Task where Id=: newComplaint.Id];
        	system.assertEquals('Completed', requeriedComplaint.Status);
        	system.assertEquals('new comments', requeriedComplaint.Description);
        	controllerObj.PhysicianId = con.Id;
        	system.assertEquals(true, controllerObj.CreateNewVisit().getUrl().contains('log_a_visit'));
        	controllerObj.ComplaintId = newComplaint.Id;
        	system.assertEquals(true, controllerObj.CloseAndCreateNewComplaint().getUrl().contains('log_a_complaint'));
        	system.assertEquals(0.0, controllerObj.SetToZeroIfNull(null));
    }
    
    
    
    static void setRecordTypes() {
    	CMAPRM__Activity_Goal__c goal = new CMAPRM__Activity_Goal__c();
		goal.CMAPRM__Goal_Year__c = String.ValueOf(Date.Today().year());
		goal.OwnerId = UserInfo.getUserId();
		goal.CMAPRM__Jan__c = 2;
		goal.CMAPRM__Feb__c = 2;
		goal.CMAPRM__Mar__c = 2;
		goal.CMAPRM__Apr__c = 2;
		goal.CMAPRM__May__c = 2;
		goal.CMAPRM__Jun__c = 2;
		goal.CMAPRM__Jul__c = 2;
		goal.CMAPRM__Aug__c = 2;
		goal.CMAPRM__Sep__c = 2;
		goal.CMAPRM__Oct__c = 2;
		goal.CMAPRM__Nov__c = 2;
		goal.CMAPRM__Dec__c = 2;
		goal.Name = 'test goal';
		
    	User sysadmin = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
		system.runAs(sysadmin) {
			system.debug('sys admin id: ' + sysadmin.Id);
			List<RecordType> recordTypes = [Select Id,SobjectType,Name From RecordType WHERE (Name ='Complaint' or Name = 'Visit' or Name='Goal') and SobjectType ='Task' ];
			for(RecordType rt : recordTypes) {
				if(rt.Name == 'Complaint') {
					ComplaintRecordTypeId = rt.Id;
					system.debug('complaint record type id is: ' + ComplaintRecordTypeId);
				}
				else if(rt.Name == 'Visit') {
					VisitRecordTypeId = rt.Id;
					system.debug('visit record type id is: ' + VisitRecordTypeId); 
				}
			}			
			//insert goal;	
		}	    	
    }
}