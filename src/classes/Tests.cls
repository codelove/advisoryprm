@isTest
private class Tests{
    static testMethod void CmaViewController(){
        test.startTest();
        
        cmaviewcontroller x = new cmaviewcontroller();
        x.seturl();
        system.assertEquals(x.cmaurl, x.CS.default_cma_url__c);
        test.stopTest();        
    }
    
    static void SetTaskPageReference(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Contact con = new Contact();
        con.AccountId = acc.Id;
        con.LastName = 'tester';
        insert con;
        
        CMA_Initiative__c ini = new CMA_Initiative__c();
        insert ini;
        
        TaskRecordTypes__c settings = TaskRecordTypes__c.getOrgDefaults(); 
        system.debug('task record type id: ' + settings.VisitTaskRecordType__c);
        PageReference pageRef = new PageReference('/00T/e');
        pageRef.getParameters().put('rtid', settings.VisitTaskRecordType__c);
        pageRef.getParameters().put('rurl', '');
        pageRef.getParameters().put('accid',acc.Id);
        pageRef.getParameters().put('cid',con.Id);
        pageRef.getParameters().put('inid',ini.Id);
        Test.setCurrentPage(pageRef);    
        system.assertEquals(settings.VisitTaskRecordType__c,ApexPages.CurrentPage().getParameters().get('rtid'));
    }
    
    static void SetComplaintPageReference() {
    	 Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Contact con = new Contact();
        con.AccountId = acc.Id;
        con.LastName = 'tester';
        insert con;
        
        CMA_Initiative__c ini = new CMA_Initiative__c();
        insert ini;
        
        TaskRecordTypes__c settings = TaskRecordTypes__c.getOrgDefaults(); 
        PageReference pageRef = new PageReference('/00T/e');
        pageRef.getParameters().put('rtid', settings.ComplaintTaskRecordType__c);
        pageRef.getParameters().put('rurl', '');
        pageRef.getParameters().put('accid',acc.Id);
        pageRef.getParameters().put('cid',con.Id);
        pageRef.getParameters().put('inid',ini.Id);
        Test.setCurrentPage(pageRef); 
        system.assertEquals(settings.ComplaintTaskRecordType__c,ApexPages.CurrentPage().getParameters().get('rtid'));   
    }
    
    @isTest(seeAllData=true)
    static void CustomTaskController_CreateTask() {
        SetTaskPageReference();
        
        test.startTest();
        
        sObject s = new Task();
        CustomTaskController x = new CustomTaskController(new ApexPages.StandardController(s));
        x.createtaskrecord();
        system.assertNotEquals(null,x.myTask);
        system.assertEquals(UserInfo.getUserID(), x.myTask.OwnerId);
        x.customsave();
        
        test.stopTest();
    }
    
    static void SetUpSettings(){
        TaskRecordTypes__c settings = new TaskRecordTypes__c();
        settings.Name = 'Test Configuration';
        settings.ComplaintTaskRecordType__c = '012i0000000BiVZ';
        settings.VisitTaskRecordType__c = '012i0000000Bi2v';
        settings.Goal_Setting_Physician__c = '003i000000Kyf8v';
        settings.Goal_Setting_Account__c = '001i000000P7L9S';
        settings.Goal_Record_Type__c = '012i0000000C3BU';
        insert settings;    
        TaskRecordTypes__c queriedSettings = TaskRecordTypes__c.getOrgDefaults(); 
        system.assertNotEquals(null, queriedSettings.VisitTaskRecordType__c);
    }
   
    
    @isTest(seeAllData=true)
    static void CustomTaskController_CreateComplaint() {
        SetComplaintPageReference();
        
        test.startTest();
        
        sObject s = new Task();
        CustomTaskController x = new CustomTaskController(new ApexPages.StandardController(s));
        x.createnewcomplaint();
        x.customsavecomplaint();
        system.assertNotEquals(null, x.newcomplaint);
        test.stopTest();
    }
    
    @isTest(seeAllData=true)
    static void CustomTaskController_SaveAndLogComplaint() {
        SetTaskPageReference();
        
        test.startTest();
        
        sObject s = new Task();
        CustomTaskController x = new CustomTaskController(new ApexPages.StandardController(s));
        x.createtaskrecord();
        x.saveandlogcomplaint();
        system.assertNotEquals(null, x.mytask);
        test.stopTest();
    }
    
    @isTest(seeAllData=true)
    static void CustomTaskController_SaveAndLogVisit() {
    	SetTaskPagereference();
    	test.StartTest();
    	sObject s =new Task();
    	CustomTaskController x = new CustomTaskController(new ApexPages.StandardController(s));
    	x.createtaskrecord();
    	x.saveandlogvisit();
    	system.assertNotEquals(null, x.mytask);
    	test.stopTest();
    }
    
    @isTest(seeAllData=true)
    static void CustomTaskController_Cancel() {
       // SetUpSettings();
        SetTaskPageReference();
        
        test.startTest();
        sObject s = new Task();
        CustomTaskController x = new CustomTaskController(new ApexPages.StandardController(s));
        x.createtaskrecord();
        x.createnewcomplaint();
        x.saveandlogcomplaint();
        x.cancelfromvisit();
        system.assertEquals(null, x.myTask);
        x.cancelfromcomplaint();
        system.assertEquals(null, x.newComplaint);
        system.assertEquals(null,x.customsave());
        system.assertEquals(null,x.saveandlogcomplaint());
        system.assertEquals(null,x.customsavecomplaint());
        test.stopTest();
    }
    /*
    @isTest(seeAllData=true)
    static void CreateGoals() {
      // SetUpSettings();
        
        test.startTest();
        
        Activity_Goal__c o= new Activity_Goal__c();
        o.Name = 'Test';
        o.Goal_Year__c = String.ValueOf(date.Today().addYears(20).Year());
        o.OwnerId = userinfo.getUserId();
        o.Applies_To__c = userinfo.getUserId();
        insert o;
        system.assertNotEquals(null, o.Id);
        test.stopTest();
    }*/
    
    static testMethod void UpdateInitPhysiciansActivityMetrics() {
        CMA_Initiative__c i= new CMA_Initiative__c();
        i.Name = 'Test';
        i.InitiativeKey__c = 1;
        insert i;
        
        Contact c = new Contact();
        c.LastName = 'Test';
        insert c;
        
        test.startTest();
        
        Task task = new Task();
        task.WhatId = i.Id;
        task.WhoId = c.Id;
        insert task;
        system.assertNotEquals(null, task.Id);
        test.stopTest();    
    }
    
    static testMethod void CustomTaskController_getErrorMessage() {
    	sObject s =new Task();
    	CustomTaskController x = new CustomTaskController(new ApexPages.StandardController(s));
    	system.assertEquals('Outreach Date value must be in the Past.', x.getErrorMessage('Outreach Date value must be in the Past'));
    }
}