public with sharing class myPhysiciansHomeController {
   
   public ApexPages.StandardSetController contactSet {        
        get {
            if(contactSet == null) {
                contactSet = new         
                ApexPages.StandardSetController(Database.getQueryLocator(
               [Select Title, Phone, Name, Id, Email, Account.Name, AccountId From Contact  where IsDeleted = false and OwnerId=: UserInfo.getUserId() order by CreatedDate desc limit 9000]));
            }
            return contactSet;
        }
        set;
    }
    public List<contact> getContacts() 
    {
         //return (List<contact>) contactSet.getRecords();
         list<contact> contacts=[Select Title, Phone, Name, Id, Email, Account.Name, AccountId From Contact  where IsDeleted = false and OwnerId=: UserInfo.getUserId() order by CreatedDate desc limit 9000];
         return contacts;
    } 
}