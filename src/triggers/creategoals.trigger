trigger creategoals on Activity_Goal__c (before update, before insert) {

	if(Trigger.IsInsert) {
		Set<Id> ownerIds = new Set<Id>();
		for(Activity_Goal__c ag: Trigger.New) {
			ownerIds.add(ag.OwnerId);
		}
		//Get existing activity goal records to examine year value to ensure uniqueness of year on activity goals
		List<CMAPRM__Activity_Goal__c> goals = [Select ID, CMAPRM__Goal_Year__c, OwnerId from CMAPRM__Activity_Goal__c where OwnerId in : ownerIds];
		Map<Id, List<String>> ownerToGoalYears = new Map<Id,List<String>>();
		for(CMAPRM__Activity_Goal__c goal : goals) {
			if(ownerToGoalYears.containsKey(goal.OwnerId)) {
				List<String> goalYears = ownerToGoalYears.get(goal.OwnerId);
				goalYears.add(goal.CMAPRM__Goal_Year__c);
				ownerToGoalYears.put(goal.OwnerId, goalYears);
			}
			else {
				List<String> goalYears = new List<String>();
				goalYears.add(goal.CMAPRM__Goal_Year__c);
				ownerToGoalYears.put(goal.OwnerId, goalYears);
			}
		}
		for(Activity_Goal__c goal : Trigger.New) {
			if(ownerToGoalYears.containsKey(goal.OwnerId)) {
				List<String> goalYears = ownerToGoalYears.get(goal.OwnerId);
				if(goalYears != null) {
					for(String year : goalYears) {
						if(goal.CMAPRM__Goal_Year__c == year) {
							goal.addError('The selected user already has an Activity Goal for the year ' + year + '. Any edits should be made to the original record.');
						}
					}
				}
			}
		}
	}
	//this trigger generates and maintains activity goal records, stored as tasks, in ine with the manuall entered goals
	
	//first confirm the existence and uniqueness of goals records
	set<decimal> years = new set<decimal>();
	set<id> assignedids = new set<id>();
	set<string> types = new set<string>();

	system.debug('user running id: ' + userinfo.getUserId());
	TaskRecordTypes__c settings = TaskRecordTypes__c.getInstance(userinfo.getUserId());
	
	//assemble list of years and staff to check
	 list<Task> unionlist = new list<Task>();
	 //deprecated list<string> details = new list<string>();
	 //details holds the following information
	 //pos 0 = month, pos 1 = year, pos 2 = type, pos 3=ownerid, pos4=goal value, pos 5 = number of matches pos 6=id of last matching task record
     list<Task> taskstodelete = new list<Task>(); 

	 decimal goalvalue=0;
	 Task temptask = new Task();
	 date tempdate;
	 string subject;
 
    
	for (activity_goal__c goal:trigger.new){
    
   	 	for (integer i=1; i<13; i++){
	        temptask= new Task();
	        //create activity date
	        tempdate=date.newInstance(integer.valueof(goal.year__c), i, 1);
	        temptask.activitydate=tempdate;
	        temptask.goal_type__c=goal.type__c;
	        temptask.OwnerId=goal.OwnerId;
	        system.debug('owner id is: ' + goal.applies_to__c);
	        system.debug('another id is: ' + goal.OwnerId);
	        temptask.recordtypeid=settings.goal_record_type__c;
	        temptask.WhatId=settings.goal_setting_account__c;
	        temptask.WhoId=settings.goal_setting_physician__c;
	        temptask.status = 'Completed';
	        temptask.purpose__c = Null;
	        subject='Goal for '+ goal.assigned_to_name__c + ' '  + string.valueof(tempdate);
	        temptask.Subject = subject;
	        if(i==1){temptask.goal__c=goal.jan__c;}
	        if(i==2){temptask.goal__c=goal.feb__c;}
	        if(i==3){temptask.goal__c=goal.mar__c;}
	        if(i==4){temptask.goal__c=goal.apr__c;}
	        if(i==5){temptask.goal__c=goal.may__c;}
	        if(i==6){temptask.goal__c=goal.jun__c;}
	        if(i==7){temptask.goal__c=goal.jul__c;}
	        if(i==8){temptask.goal__c=goal.aug__c;}
	        if(i==9){temptask.goal__c=goal.sep__c;}
	        if(i==10){temptask.goal__c=goal.oct__c;}
	        if(i==11){temptask.goal__c=goal.nov__c;}
	        if(i==12){temptask.goal__c=goal.dec__c;}
   
      
        	unionlist.add(temptask);
      	}
	    if (!years.contains(goal.year__c)){
	    	years.add(goal.year__c);
	    }
    
    	if (!assignedids.contains(goal.applies_to__c)){
    		assignedids.add(goal.applies_to__c);
    	}
    
    	if (!types.contains(goal.type__c)){
       		types.add(goal.type__c);
       	}    
	}

	//retrieve all goal records for the time period and staff affected
	
	list<Task> goaltasks = new list<Task>();
	string rtid = settings.goal_record_type__c;
	goaltasks = [Select OwnerId, WhoId, WhatID, ActivityDate, Activity_year__c, Goal_type__c, Activity_date_value__c, Goal__c, Status, Type, RecordtypeId From Task Where activity_year__c in :years and OwnerId in :assignedids and goal_type__c in :types and RecordtypeId=:rtid];
    
	system.debug('the size of the existing task list is:');
	system.debug(goaltasks.size());
	system.debug('end of message'); 
	 
	 integer matchcounter=0; 
	 id lastmatchid = Null;
 
	 //check for existence and gap-fill
	 //each sub-list has positions 0-4 occupied and positions 5 and 6 will be filled during this process
	 
	integer positioncounter=0; 
	system.debug('unionlist size is: ' + unionlist.size());
	system.debug('goal task list size is: ' + goalTasks.size());
	for (Task desiredtask:unionlist){
    //if task matches, replace
        for (Task checktask:goaltasks){
            system.debug('checking if task matches');
            matchcounter=0;
            if(checktask.ownerid==desiredtask.ownerid && checktask.activitydate.year()==desiredtask.activitydate.year() && checktask.activitydate.month()==desiredtask.activitydate.month() && checktask.goal_type__c==desiredtask.goal_type__c){
                system.debug('task matched!');
                checktask.goal__c=desiredtask.goal__c;
                checktask.whoid=desiredtask.whoid;
                checktask.whatid=desiredtask.whatid;
                checktask.subject=desiredtask.subject;
                unionlist.set(positioncounter,checktask);
                if(matchcounter>0){
                	taskstodelete.add(checktask);
             	}
             	++matchcounter;
         	}
         	else {
         		system.debug('task did not match');
         	}
		}
    	++positioncounter;
    }
         
	 upsert unionlist;      
	 delete taskstodelete;
}