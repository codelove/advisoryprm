trigger Contact on Contact (after insert,after update, after delete) {
	if((Trigger.IsInsert || Trigger.IsDelete) && Trigger.IsAfter) {
		//Build a list of Account Ids associated with the contacts
		Set<Id> AccountIds = new Set<Id>(); 
		List<Contact> triggerContacts;
		if(Trigger.isInsert || Trigger.isUpdate) {
			triggerContacts = Trigger.New;
		}
		else {
			triggerContacts = Trigger.Old;
		}
		for(Contact c: triggerContacts) {
			if(Trigger.IsInsert || Trigger.IsDelete || (Trigger.isUpdate && (Trigger.oldMap.get(c.Id).AccountId != c.AccountId ))) {
				AccountIds.add(c.AccountId);
			}
		}		
		Map<Id,Integer> AccContactCount = new Map<Id,Integer>();
		//Get the existing contact count for each account effected by the contacts added in this trigger
		List<Contact> contacts = [Select Id, AccountId from Contact where AccountId in : AccountIds];	
		
		for(Contact c : contacts) {
			if(c.AccountId != null) {
				if(AccContactCount.containsKey(c.AccountId)) {
					Integer count = AccContactCount.get(c.AccountId);
					AccContactCount.put(c.AccountId, count+1);
				}
				else {
					AccContactCount.put(c.AccountId, 1);
				}
			}
		}
		
		//Update the physician count field on accounts
		List<Account> accsToUpdate = [Select Id, PhysicianCount__c from Account where Id in : AccContactCount.keyset()];
		if(accsToUpdate != null && accsToUpdate.size() > 0) {
			for(Account acc : accsToUpdate) {
				if(AccContactCount.containsKey(acc.Id)) {
					acc.CMAPRM__PhysicianCount__c = AccContactCount.get(acc.Id);
				}				
			}
			update accsToUpdate;
		}
	}
}