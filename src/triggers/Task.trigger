trigger Task on Task (after insert, after update) {
    
    list<Task> tasks = New list<Task>();
    list<Id> initid = new list<Id>();
    list<Id> docid = new list<Id>();
    
    string recordTypeId = [Select Id From RecordType Where Name ='Visit' and SobjectType ='Task' limit 1].Id;
    
    //Build a list of physician Ids
    List<Id> physicianids = new List<Id>();   
    for(Task t: Trigger.new){  
    	if(t.Whatid <> Null){
            tasks.add(t);
            initid.add(t.whatid);
            docid.add(t.whoid);
        }
        if (t.RecordTypeId == recordTypeId) {
            physicianids.add(t.WhoId);
        }
    }
       
    //Set physician's fields from the latest task associated
    list<Contact> physicians = [Select Id,CMAPRM_LastVisitedDate__c,CMAPRM_LastVisitedBy__c From Contact Where Id In:physicianids];
    //return all the possibly related tasks
    list<Task> alltasks = [Select Id, WhoId, WhatId, Counts_As_Outreach_Activity__c From Task where WhatId in :initid AND WhoId in:docid];
    //return initiatives related to the tasks
    list<CMA_Initiative__c> inits = [Select Id,CMAPRM__Total_Outreach_Activities__c, (Select Id, Contacted_During_Initiative__c, Physician__c, Outreach_activities_during_initiative__c From CMA_Initiative__c.InitiativePhysicians__r Where Physician__r.Id in :docid) From CMA_Initiative__c where Id in :initid];
    list<Task> initTasks = [Select Id, WhatId from Task Where WhatId in :initid];
    
    
    Map<string, Id> taskRecordTypesNames_Id=UtilsRecordTypes.getMapRecordTypeNmAndId('Task');
    
    system.debug(physicians.size());
    For(Contact p:physicians){
        For(Task t: Trigger.New){
            if(t.WhoId == p.Id) 
            {
            	if(t.RecordTypeId==taskRecordTypesNames_Id.get('Visit'))
            	{
            		if (t.CMAPRM__Outreach_Date_Value__c>p.CMAPRM_LastVisitedDate__c || p.CMAPRM_LastVisitedDate__c==NULL )
            		{
	                    p.CMAPRM_LastVisitedDate__c=t.CMAPRM__Outreach_Date_Value__c;
	                    p.CMAPRM_LastVisitedBy__c=t.OwnerId;
	                }
            	}
            	else
            	{
	                if (t.ActivityDate>p.CMAPRM_LastVisitedDate__c || p.CMAPRM_LastVisitedDate__c==NULL ){
	                    p.CMAPRM_LastVisitedDate__c=t.ActivityDate;
	                    p.CMAPRM_LastVisitedBy__c=t.OwnerId;
	                } 
            	}
            }   
        }  
    }
        
    Update physicians;
    
    List<CMA_InitiativePhysician__c> allDocs = new List<CMA_InitiativePhysician__c>();
    for(CMA_Initiative__c i:inits){
    	system.debug('in initiatives loop');
    	List<CMA_InitiativePhysician__c> docs = i.InitiativePhysicians__r;
        system.debug(docs.size());
        //if(docs.size()>0){
        for(CMA_InitiativePhysician__c ip:docs){
        	ip.Contacted_During_Initiative__c=0;
            ip.Outreach_activities_during_initiative__c=0;
            for(Task taskloop:alltasks){
            	system.debug('entering all tasks loop');
            	system.debug('task loops count as outreach activity :' + taskloop.Counts_As_Outreach_Activity__c );
            	if(taskloop.WhatID==i.ID && taskloop.WhoId==ip.Physician__c){
                	system.debug('im in the loop');
                    ip.Contacted_During_Initiative__c=1;
                    ip.Outreach_activities_during_initiative__c=++ip.Outreach_activities_during_initiative__c;
                }
            }
     	}
        allDocs.addAll(docs);
  	}
  	update allDocs;
  	
  	for(CMA_Initiative__c i : inits) {
  		integer totaloutreachActivities = 0;
  		for(Task t : initTasks) {
  			if(t.whatid == i.Id) {
  				totaloutreachActivities ++;
  			}
  		}
  		i.CMAPRM__Total_Outreach_Activities__c = totaloutreachActivities;
  	}
  	
  	update inits;
  	
}